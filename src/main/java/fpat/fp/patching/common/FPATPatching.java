package fpat.fp.patching.common;

import java.io.File;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerAboutToStartEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;


@Mod(modid = FPATPatching.modID, name = FPATPatching.modName, version = FPATPatching.modVersion, dependencies = "required-after:fp;")
public class FPATPatching
{
	public static final String modID = "fpatpatching";
	public static final String modName = "Futurepack-Patching";
	public static final String modVersion = "Version.version";
	
	@Instance(FPATPatching.modID)
	public static FPATPatching instance;
	
	public FPATPatching()
	{
		
	}


	@SidedProxy(modId=FPATPatching.modID, clientSide="fpat.fp.patching.common.FPATPatchingProxyClient", serverSide="fpat.fp.patching.common.FPATPatchingProxyServer")
	public static FPATPatchingProxyBase proxy;
	
	
	@EventHandler
    public void preInit(FMLPreInitializationEvent event) 
    {	
    	proxy.preInit(event);
    }
	
	@EventHandler
	public void load(FMLInitializationEvent event)
	{
		proxy.load(event);
	}
	
	@EventHandler
	public void postInit(FMLPostInitializationEvent event)
	{
		proxy.postInit(event);
	}
	
	@EventHandler
	public void preServerStart(FMLServerAboutToStartEvent event) 
	{
		
	}
	
	@EventHandler
	public void serverStarting(FMLServerStartingEvent event) 
	{		

	}
	
	@EventHandler
	public void serverStarted(FMLServerStartedEvent event) 
	{

	}
	
	
	@EventHandler
	public void serverStopped(FMLServerStoppingEvent event) 
	{

	}
}