package fpat.fp.patching.common;

import java.util.ArrayList;
import java.util.List;

import fpat.fp.patching.research.FPATPatchingResearchLoader;
import futurepack.common.FPLog;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.fml.common.ModMetadata;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class FPATPatchingProxyBase {

	
	public void preInit(FMLPreInitializationEvent event) 
	{
		FPLog.initLog();
	
	}

	public void load(FMLInitializationEvent event)
	{
	    FPATPatchingResearchLoader.init();
	}
	
	public void postInit(FMLPostInitializationEvent event)
	{
		
	}
	
}
