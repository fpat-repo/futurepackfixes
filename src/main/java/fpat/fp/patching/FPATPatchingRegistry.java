package fpat.fp.patching;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import fpat.fp.patching.common.FPATPatching;
import fpat.fp.patching.research.icons.FPATPatchingResearchIcons;

@Mod.EventBusSubscriber(modid = FPATPatching.modID)
public class FPATPatchingRegistry {
	
	@SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
//		FPCABlocks.register(event);
	}
	
	@SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event)
	{
//		event.getRegistry().registerAll(FPCABlocks.itemBlocks.toArray(new Item[FPCABlocks.itemBlocks.size()]));
//		FPCABlocks.itemBlocks.clear();
//		FPCABlocks.itemBlocks = null;
		
		FPATPatchingResearchIcons.register(event);
	}
	
	@SubscribeEvent
	public static void registerModels(ModelRegistryEvent event)
	{
		try
		{
//			FPCABlocks.setupPreRendering();
		}
		catch(NoSuchMethodError e){}
		FPATPatchingResearchIcons.setupRendering(); 
	}
}
