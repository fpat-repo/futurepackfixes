package fpat.fp.patching.research;

import fpat.fp.patching.research.icons.FPATPatchingResearchIcons;
import fpat.fp.patching.common.FPATPatching;
import futurepack.api.event.ResearchPageRegisterEvent;
import futurepack.common.item.FPItems;
import futurepack.common.research.ResearchLoader;
import futurepack.common.research.ResearchPage;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = FPATPatching.modID)
public class FPATPatchingResearchPage
{

	public static ResearchPage fpatpatching;
	
	/**
	 * Used in {@link ResearchLoader} to make sure, <i>/fp research reload</i>, works.
	 */
	
	@SubscribeEvent
    public static void init(ResearchPageRegisterEvent event)
	{
	};
	
}
