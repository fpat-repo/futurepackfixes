package fpat.fp.patching.research;

import java.io.InputStreamReader;

import futurepack.common.FPLog;
import futurepack.common.research.ResearchLoader;

public class FPATPatchingResearchLoader
{
	
	public static void init()
	{
		try
		{
			ResearchLoader.instance.addResearchesFromReader(new InputStreamReader(FPATPatchingResearchLoader.class.getResourceAsStream("research.json")));
		}
		catch(Exception ex)
		{
			FPLog.logger.error("Failed to load research.json for FPATPatcher!");
			ex.printStackTrace();
		}
	}
}
